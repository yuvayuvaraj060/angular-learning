import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChildRouteGuardComponent } from './child-route-guard/child-route-guard.component';
import { DirectivesComponent } from './directives/directives.component';
import { NestedFrormComponent } from './nested-frorm/nested-frorm.component';
import { PostComponent } from './posts/post/post.component';
import { PostsComponent } from './posts/posts.component';
import { ProtucedRouteComponent } from './protuced-route/protuced-route.component';
import { ReactiveFormComponent } from './reactive-form/reactive-form.component';
import {
  AuthGardService,
  ChildResolve,
} from './shared/services/auth-gard.service';
import { GardChildService } from './shared/services/gard-child.service';
import { SubjectComponent } from './subject/subject.component';
import { TempletFormComponent } from './templet-form/templet-form.component';
import { TopCommponentComponent } from './top-commponent/top-commponent.component';

const routes: Routes = [
  {
    path: '',
    component: PostsComponent,
  },
  { path: 'post', component: PostComponent },
  {
    path: 'template-form',
    component: TempletFormComponent,
  },
  {
    path: 'reactive-from',
    component: ReactiveFormComponent,
  },
  {
    path: 'nested-reactive-form',
    component: NestedFrormComponent,
  },
  {
    path: 'structural-directives',
    component: DirectivesComponent,
  },
  {
    path: 'component-splitting',
    component: TopCommponentComponent,
  },
  {
    path: 'subject',
    component: SubjectComponent,
  },
  {
    path: 'protected',
    component: ProtucedRouteComponent,
    canActivate: [AuthGardService],
    canActivateChild: [GardChildService],
    children: [
      {
        path: 'child',
        component: ChildRouteGuardComponent,
        resolve: { userData: ChildResolve },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: [],
})
export class AppRoutingModule {}
