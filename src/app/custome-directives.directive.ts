import { Directive, ElementRef, HostBinding, HostListener } from '@angular/core';

@Directive({
  selector: '[appCustomeDirectives]',
})
export class CustomeDirectivesDirective {
  constructor(private el: ElementRef) {}
count : number = 0;
  @HostListener('mouseenter') onMouseEnter() {
    this.highlight('red');
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.highlight('green');
  }
  @HostBinding('style.border') border: string;
  @HostListener('click') onClick() {
    this. addCount();
    this.border = '15px solid blue'
  }
  private highlight(color: string) {
    this.el.nativeElement.style.backgroundColor = color;
  }

  private addCount() {
    this.count++
    console.log(this.count);
    alert(this.count)
  }
}
