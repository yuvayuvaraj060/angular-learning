import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NestedFrormComponent } from './nested-frorm.component';

describe('NestedFrormComponent', () => {
  let component: NestedFrormComponent;
  let fixture: ComponentFixture<NestedFrormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NestedFrormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NestedFrormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
