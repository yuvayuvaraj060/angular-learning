import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-nested-frorm',
  templateUrl: './nested-frorm.component.html',
  styleUrls: ['./nested-frorm.component.scss'],
})
export class NestedFrormComponent implements OnInit {
  myForm: FormGroup;
  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    const phone = this.fb.group({
      area: [],
      prefix: [],
      line: [],
    });
    this.myForm = this.fb.group({
      email: '',
      homePhone: phone,
      personalPhone: phone,
    });
  }

  onSubmit() {
    console.log(this.myForm.value);
  }
}
