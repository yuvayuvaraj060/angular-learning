import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {
  BrowserAnimationsModule,
  ɵBrowserAnimationBuilder,
} from '@angular/platform-browser/animations';
import { PostsComponent } from './posts/posts.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MaterialModule } from './material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TempletFormComponent } from './templet-form/templet-form.component';
import { ReactiveFormComponent } from './reactive-form/reactive-form.component';
import { NestedFrormComponent } from './nested-frorm/nested-frorm.component';
import { DirectivesComponent } from './directives/directives.component';
import { CustomeDirectivesDirective } from './custome-directives.directive';
import { TopCommponentComponent } from './top-commponent/top-commponent.component';
import { ChildCommponentComponent } from './top-commponent/child-commponent/child-commponent.component';
import { SubjectComponent } from './subject/subject.component';
import { LearnInterceptor } from './interceptor/httpconfig.interceptor';
import { APICallService } from './shared/services/API-call.service';
import { SubjectMessageServiceService } from './shared/services/subjectMessageService.service';
import { ProtucedRouteComponent } from './protuced-route/protuced-route.component';
import { ChildRouteGuardComponent } from './child-route-guard/child-route-guard.component';
import { GardChildService } from './shared/services/gard-child.service';
import { AuthService } from './shared/services/auth.service';
import { AuthGardService } from './shared/services/auth-gard.service';
import { PostComponent } from './posts/post/post.component';

@NgModule({
  declarations: [
    AppComponent,
    PostsComponent,
    TempletFormComponent,
    ReactiveFormComponent,
    NestedFrormComponent,
    DirectivesComponent,
    CustomeDirectivesDirective,
    TopCommponentComponent,
    ChildCommponentComponent,
    SubjectComponent,
    ProtucedRouteComponent,
    ChildRouteGuardComponent,
    PostComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [
    APICallService,
    SubjectMessageServiceService,
    { provide: HTTP_INTERCEPTORS, useClass: LearnInterceptor, multi: true },
    GardChildService,
    AuthGardService,
    AuthService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
