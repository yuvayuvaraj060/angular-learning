import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Post } from 'src/app/shared/interfaces/post';
import { APICallService } from 'src/app/shared/services/API-call.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss'],
})
export class PostComponent implements OnInit {
  post: Post;
  params: { id: string };
  constructor(
    private apiCallServices: APICallService,
    private activatedRoutes: ActivatedRoute
  ) {
    this.activatedRoutes.queryParams.subscribe((params: any) => {
      this.params = params;
      console.log(params);
    });
  }

  ngOnInit() {
    this.getPost(this.params.id);
  }

  getPost(id: string) {
    this.apiCallServices
      .getSinglePost(id)
      .subscribe((post) => (this.post = post));
  }
}
