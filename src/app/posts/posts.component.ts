import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Post } from '../shared/interfaces/post';
import { APICallService } from '../shared/services/API-call.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss'],
})
export class PostsComponent implements OnInit {
  posts: Post[] | null = null;

  constructor(private apiCallServices: APICallService) {}

  ngOnInit() {
    this.loadPosts();
  }

  loadPosts() {
    this.apiCallServices.getAllPosts().subscribe((posts) => {
      this.posts = posts;
    });
  }
}
