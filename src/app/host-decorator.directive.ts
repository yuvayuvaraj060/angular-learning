import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appHostDecorator]',
})
export class HostDecoratorDirective {
  numberOfClicks: number = 0;
  constructor(private el: ElementRef) {}

  @HostListener('click') onClink() {
    console.log('button', 'number of clicks:', this.numberOfClicks++);
    alert('Count' + this.numberOfClicks);
  }
}
