import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopCommponentComponent } from './top-commponent.component';

describe('TopCommponentComponent', () => {
  let component: TopCommponentComponent;
  let fixture: ComponentFixture<TopCommponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopCommponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopCommponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
