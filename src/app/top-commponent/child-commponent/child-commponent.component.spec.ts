import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildCommponentComponent } from './child-commponent.component';

describe('ChildCommponentComponent', () => {
  let component: ChildCommponentComponent;
  let fixture: ComponentFixture<ChildCommponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildCommponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildCommponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
