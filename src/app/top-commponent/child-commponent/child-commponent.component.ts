import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-child-commponent',
  templateUrl: './child-commponent.component.html',
  styleUrls: ['./child-commponent.component.scss'],
})
export class ChildCommponentComponent implements OnInit {
  @Input() dataFromTopComponent: { name: string }[];
  @Output() displayData = new EventEmitter();
  @Output() hideData = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}
}
