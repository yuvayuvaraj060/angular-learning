import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-top-commponent',
  templateUrl: './top-commponent.component.html',
  styleUrls: ['./top-commponent.component.scss'],
})
export class TopCommponentComponent implements OnInit {
  names: { name: string }[];
  displayData: boolean = false;
  constructor() {}

  ngOnInit(): void {
    this.names = [{ name: 'yuvi' }, { name: 'hari' }];
  }

  display(event: boolean): void {
    this.displayData = event;
  }

  hide(event: boolean): void {
    this.displayData = event;
  }
}
