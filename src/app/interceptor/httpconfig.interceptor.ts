import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
} from '@angular/common/http';

import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

/** Pass untouched request through to the next request handler. */
@Injectable()
export class LearnInterceptor implements HttpInterceptor {
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    console.log(
      '🚀 ~ file: httpconfig.interceptor.ts ~ line 18 ~ LearnInterceptor ~ req',
      req
    );

    const addRootUrl = req.clone({
      url: `https://jsonplaceholder.typicode.com/${req.url}`,
    });

    const addToken = addRootUrl.clone({
      headers: req.headers.set('Authorization', 'done close'),
    });
    return next.handle(addToken).pipe(
      tap(
        (data) => {
          console.log('Success', data);
        },
        (err) => {
          console.log('Failed', err);
        }
      )
    );
  }
}
