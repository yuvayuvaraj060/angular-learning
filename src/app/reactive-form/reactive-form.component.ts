import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { GenderFrom } from '../shared/interfaces/formData';

@Component({
  selector: 'app-reactive-form',
  templateUrl: './reactive-form.component.html',
  styleUrls: ['./reactive-form.component.scss'],
})
export class ReactiveFormComponent implements OnInit {
  myForm: FormGroup;
  genderSelect: GenderFrom[] = [
    {
      value: 'Male',
      key: 'male',
    },
    {
      value: 'Female',
      key: 'female',
    },
  ];
  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.myForm = this.fb.group({
      name: this.fb.control(''),
      email: this.fb.control(''),
      gender: this.fb.control(''),
    });
    this.myForm.valueChanges.pipe(debounceTime(500)).subscribe(console.log);
  }
  onSubmit() {
    console.log(this.myForm);
  }
}
