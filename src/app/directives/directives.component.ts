import { Component, OnInit } from '@angular/core';
import { NgFor } from '../shared/interfaces/directives';

@Component({
  selector: 'app-directives',
  templateUrl: './directives.component.html',
  styleUrls: ['./directives.component.scss'],
})
export class DirectivesComponent implements OnInit {
  ngIf: boolean = true;

  ngForData: NgFor[] = [{ data: 'hello' }, { data: 'How Are You?' }];

  ngSwitch: string = 'second';

  // attribute directives
  attribute: string = 'Default Values';
  constructor() {}

  ngOnInit(): void {}
}
