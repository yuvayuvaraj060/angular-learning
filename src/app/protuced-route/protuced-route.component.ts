import { Component, OnInit } from '@angular/core';
import { AuthGardService } from '../shared/services/auth-gard.service';

@Component({
  selector: 'app-protuced-route',
  templateUrl: './protuced-route.component.html',
  styleUrls: ['./protuced-route.component.scss'],
})
export class ProtucedRouteComponent implements OnInit {
  auth: boolean;
  constructor(private authGardService: AuthGardService) {}

  ngOnInit() {
    this.isAuthenticated();
  }

  isAuthenticated() {
    this.auth = this.authGardService.canActivate();
  }
}
