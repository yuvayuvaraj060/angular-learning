import { Component, OnInit } from '@angular/core';
import { SubjectMessageServiceService } from '../shared/services/subjectMessageService.service';

@Component({
  selector: 'app-subject',
  templateUrl: './subject.component.html',
  styleUrls: ['./subject.component.scss'],
})
export class SubjectComponent implements OnInit {
  currentMessageStream: string;
  formData: { message: string } = {
    message: '',
  };
  constructor(private messageServices: SubjectMessageServiceService) {}

  ngOnInit(): void {
    this.getUpdatedMessage();
  }

  getUpdatedMessage() {
    this.messageServices.message.subscribe((res) => {
      console.log('mss', res);
      this.currentMessageStream = res;
    });
  }

  setMessage(message: string) {
    console.log("calling", message)
    this.messageServices.setMessage(message);
  }
}
