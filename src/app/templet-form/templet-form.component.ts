import { Component, OnInit } from '@angular/core';
import { FormData } from '../shared/interfaces/formData';

@Component({
  selector: 'app-templet-form',
  templateUrl: './templet-form.component.html',
  styleUrls: ['./templet-form.component.scss'],
})
export class TempletFormComponent implements OnInit {
  formData: FormData = {
    email: '',
    phone: '',
    age: null,
  };
  constructor() {}

  ngOnInit() {
    this.resetFrom();
  }

  resetFrom(): void {
    const formDataReset: FormData = {
      email: '',
      phone: '',
      age: null,
    };
    this.formData = formDataReset;
  }

  submit(fromData: FormData) {
    this.resetFrom();
    console.log(fromData);
  }
}
