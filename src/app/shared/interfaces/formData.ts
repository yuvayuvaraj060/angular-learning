export interface FormData {
  email: string;
  phone: string;
  age: number | null;
}

export interface GenderFrom {
  value: string;
  key: string;
}
