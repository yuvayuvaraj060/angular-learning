import { Injectable } from '@angular/core';
import { CanActivateChild } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class GardChildService implements CanActivateChild {
  constructor() {}

  canActivateChild(): boolean {
    return true;
  }
}
