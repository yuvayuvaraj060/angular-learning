/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { SubjectMessageServiceService } from './subjectMessageService.service';

describe('Service: SubjectMessageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SubjectMessageServiceService]
    });
  });

  it('should ...', inject([SubjectMessageServiceService], (service: SubjectMessageServiceService) => {
    expect(service).toBeTruthy();
  }));
});
