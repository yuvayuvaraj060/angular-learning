/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { AuthGardService } from './auth-gard.service';

describe('Service: AuthGard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthGardService]
    });
  });

  it('should ...', inject([AuthGardService], (service: AuthGardService) => {
    expect(service).toBeTruthy();
  }));
});
