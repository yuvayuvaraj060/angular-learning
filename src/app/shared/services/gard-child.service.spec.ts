/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { GardChildService } from './gard-child.service';

describe('Service: GardChild', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GardChildService]
    });
  });

  it('should ...', inject([GardChildService], (service: GardChildService) => {
    expect(service).toBeTruthy();
  }));
});
