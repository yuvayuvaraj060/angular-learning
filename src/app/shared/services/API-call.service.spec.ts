/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { APICallService } from './API-call.service';

describe('Service: APICall', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [APICallService]
    });
  });

  it('should ...', inject([APICallService], (service: APICallService) => {
    expect(service).toBeTruthy();
  }));
});
