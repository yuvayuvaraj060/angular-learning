import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Post } from '../interfaces/post';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class APICallService {
  model: string = 'posts';
  constructor(private http: HttpClient) {}

  getAllPosts(): Observable<Post[]> {
    return this.http.get<Post[]>(`${this.model}`);
  }

  getSinglePost(id: string): Observable<Post> {
    return this.http.get<Post>(`${this.model}/${id}`);
  }
}
